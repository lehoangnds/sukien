-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th10 19, 2017 lúc 02:57 AM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_sukien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `color_price`
--

CREATE TABLE `color_price` (
  `id` int(11) NOT NULL,
  `vip_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `color_price`
--

INSERT INTO `color_price` (`id`, `vip_name`, `color`, `price`, `created`, `updated`) VALUES
(1, 'vip1', '#BB0000', 3000000, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'vip2', '00FFFF', 200000, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'vip3', '', 0, NULL, NULL),
(4, 'vip4', '', 0, NULL, NULL),
(5, 'vip5', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `color_price_id` int(11) NOT NULL,
  `setting` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `color_price_id`, `setting`, `created`, `updated`) VALUES
(1, 1, '{row: \"A-M\", column: \'10-28|9-27\'}', '2017-10-15 00:00:00', '2017-10-15 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `row`
--

CREATE TABLE `row` (
  `id` int(11) NOT NULL,
  `row_name` varchar(10) NOT NULL,
  `chair_column` varchar(30) NOT NULL,
  `color_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `row`
--

INSERT INTO `row` (`id`, `row_name`, `chair_column`, `color_id`, `created`, `updated`) VALUES
(1, 'A', '10-28|9-27', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(2, 'A', '2-8|1-7', 2, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(3, 'B', '12-30|11-29', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(4, 'B', '4-10|3-9', 2, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(5, 'B', '2-|1-', 3, '2017-10-19 07:27:33', '0000-00-00 00:00:00');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `color_price`
--
ALTER TABLE `color_price`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `row`
--
ALTER TABLE `row`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `color_price`
--
ALTER TABLE `color_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `row`
--
ALTER TABLE `row`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
