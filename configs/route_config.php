<?php  
	class Route {
		private $_uri = array();
		private $_method = array();

		// them moi 1 uri
		public function add($uri, $method = null){
			$this->_uri[] = trim($uri,'/');
			if($method != null){
				$arr = explode(".", $method);
				$this->_method[] = $arr;
			}
		}

		// submit
		public function submit(){
			$param = isset($_GET['uri']) ? $_GET['uri'] : '/';
			$param = trim($param,'/');
			foreach ($this->_uri as $key => $value) {
				# code...
				if(preg_match("#^$value$#", $param)){
					$useClass = $this->_method[$key][0];
					$class = new $useClass();
					$method = $this->_method[$key][1];
					$class-> $method();
				}
			}
		}
	}
	
?>