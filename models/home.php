<?php  
	require_once __DIR__."/../configs/db.php";

	class HomeModle extends Database {
		function get_role (){
			$sql = "select r.*,c.* from role r
					inner join color_price c on r.color_price_id = c.id";
			$qr = $this->conn->query($sql);
			return $qr->fetch_all(MYSQLI_ASSOC);
		}

		function get_row(){
			$sql = "select r.*, c.* from row r inner join color_price c on c.id = r.color_id";
			$qr = $this->conn->query($sql);
			return $qr->fetch_all(MYSQLI_ASSOC);
		}
	}


?>