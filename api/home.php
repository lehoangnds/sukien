<?php  
	require_once "models/home.php";
	class Home{
		public $model;
		public function __construct(){
			$this->model = new HomeModle();
		}

		function index(){
			require_once 'views/index.php';
		}

		function select_chair(){
			$row = $this->model->get_row();
			// echo "<pre>";
			// print_r($row);
			require_once 'views/home/show_chair.php';
		}

	}


?>