<?php  
	// include route config
	include 'configs/route_config.php';

	// include class tuong ung voi rotue
	include 'api/home.php';
	include 'api/admin.php';

	// khoi tao class Route
	$route = new Route();

	// Them moi 1 route
	$route->add('/', 'Home.index');// ten routes va class.function
	$route->add('/chon-ghe', 'Home.select_chair');
	$route->add('/cai-dat-ghe', 'Admin.set_chair_view');
	$route->add('/admin', 'Admin.index');

	//submit
	$route->submit();
	
?>